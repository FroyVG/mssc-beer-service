package info.froylanvillaverde.msscbeerservice.services;

import info.froylanvillaverde.msscbeerservice.domain.Beer;
import info.froylanvillaverde.msscbeerservice.repo.BeerRepository;
import info.froylanvillaverde.msscbeerservice.web.mappers.BeerMapper;
import info.froylanvillaverde.msscbeerservice.web.model.BeerDto;
import info.froylanvillaverde.msscbeerservice.web.model.BeerStyleEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
public class BeerService {

    @Autowired
    private BeerRepository repository;

    @Autowired
    private BeerMapper mapper;

    public BeerDto getBeerById(UUID beerId) {

        Optional<Beer> beer = repository.findById(beerId);
        Optional<BeerDto> beerDto = beer.map(mapper::beerToBeerDto);

        return beerDto.get();
    }

    public BeerDto saveNewBeer(BeerDto beerDto) {
        Beer beer = mapper.beerDtoToBeer(beerDto);
        beer = repository.save(beer);
        beerDto = mapper.beerToBeerDto(beer);
        return beerDto;
    }

    public void updateBeer(UUID beerId, BeerDto beerDto) {
        log.info("Updating....");
        repository.findById(beerId)
                .ifPresent(beer -> {
                    beer.setBeerName(beerDto.getBeerName());
                    beer.setBeerStyle(beer.getBeerStyle());
                    beer.setPrice(beerDto.getPrice());
                    beer.setUpc(beerDto.getUpc());
                    repository.save(beer);
                });
    }

    public void deleteById(UUID beerId) {
        log.debug("Deleting a beer...");
        Optional<Beer> beerOptional = repository.findById(beerId);
        beerOptional.ifPresent(beer -> {
            repository.delete(beer);
        });
    }
}
