package info.froylanvillaverde.msscbeerservice.services;

import info.froylanvillaverde.msscbeerservice.domain.Customer;
import info.froylanvillaverde.msscbeerservice.repo.CustomerRepository;
import info.froylanvillaverde.msscbeerservice.web.mappers.CustomerMapper;
import info.froylanvillaverde.msscbeerservice.web.model.CustomerDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
public class CustomerService {
    @Autowired
    private CustomerMapper customerMapper;

    @Autowired
    private CustomerRepository repository;

    public CustomerDto getCustomerById(UUID customerId) {
        return repository.findById(customerId)
                .map(customerMapper::customerToCustomerDto)
                .get();
    }

    public CustomerDto saveNewCustomer(CustomerDto customerDto) {
        Customer customer = customerMapper.customerDtoToCustomer(customerDto);
        customer = repository.save(customer);
        customerDto = customerMapper.customerToCustomerDto(customer);
        return customerDto;
    }

    public void updateCustomer(UUID customerId, CustomerDto customerDto) {
        //todo impl
        log.debug("Updating....");
        repository.findById(customerId)
                .ifPresent(customer -> {
                    customer.setName(customer.getName());
                });
    }

    public void deleteById(UUID customerId) {
        log.debug("Deleting.... ");
        Optional<Customer> customerOptional = repository.findById(customerId);
        customerOptional.ifPresent(customer -> {
            repository.delete(customer);
        });
    }
}
