package info.froylanvillaverde.msscbeerservice.events;

import info.froylanvillaverde.msscbeerservice.web.model.BeerDto;

public class BrewBeerEvent extends BeerEvent {

    public BrewBeerEvent(BeerDto beerDto) {
        super(beerDto);
    }

}
