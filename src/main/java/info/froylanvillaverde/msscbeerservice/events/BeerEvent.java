package info.froylanvillaverde.msscbeerservice.events;

import info.froylanvillaverde.msscbeerservice.web.model.BeerDto;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;

@Data
@RequiredArgsConstructor
@Builder
public class BeerEvent implements Serializable {

    static final long serialVersionUID= -109968273808062799L;
    private final BeerDto beerDto;
}
