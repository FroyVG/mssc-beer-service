package info.froylanvillaverde.msscbeerservice.events;

import info.froylanvillaverde.msscbeerservice.web.model.BeerDto;

public class NewInventoryEvent extends BeerEvent {
    public NewInventoryEvent(BeerDto beerDto) {
        super(beerDto);
    }
}
