package info.froylanvillaverde.msscbeerservice.bootstrap;

import info.froylanvillaverde.msscbeerservice.domain.Beer;
import info.froylanvillaverde.msscbeerservice.domain.Customer;
import info.froylanvillaverde.msscbeerservice.repo.BeerRepository;
import info.froylanvillaverde.msscbeerservice.repo.CustomerRepository;
import info.froylanvillaverde.msscbeerservice.web.model.BeerStyleEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.UUID;

@Component
@Slf4j
public class BeerLoader implements CommandLineRunner {

    public static final String BEER_UPC_1 = "0979798798789";
    public static final String BEER_UPC_2 = "7678698790809";
    public static final String BEER_UPC_3 = "0989789876786";

    private final BeerRepository beerRepository;

    private final CustomerRepository customerRepository;

    public BeerLoader(BeerRepository beerRepository, CustomerRepository customerRepository) {
        this.beerRepository = beerRepository;
        this.customerRepository = customerRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        if (beerRepository.count() == 0 ){
            loadBeerObjects();
        }
    }

    private void loadBeerObjects() {
            Beer b1 = Beer.builder()
                            .beerName("Mango bobs")
                            .beerStyle(BeerStyleEnum.IPA.name())
                            .quantityToBrew(200)
                            .minOnHand(12)
                            .upc(BEER_UPC_1)
                            .price(BigDecimal.valueOf(12.95))
                            .build();
            Beer b2 = Beer.builder()
                            .beerName("Corona")
                            .beerStyle(BeerStyleEnum.ALE.name())
                            .quantityToBrew(200)
                            .minOnHand(12)
                            .upc(BEER_UPC_2)
                            .price(BigDecimal.valueOf(11.95))
                            .build();
            Beer b3 = Beer.builder()
                            .beerName("Modelo")
                            .beerStyle(BeerStyleEnum.GOSE.name())
                            .quantityToBrew(100)
                            .minOnHand(10)
                            .upc(BEER_UPC_3)
                            .price(BigDecimal.valueOf(9.95))
                            .build();

        beerRepository.save(b1);
        beerRepository.save(b2);
        beerRepository.save(b3);

        if (customerRepository.count()==0){
            customerRepository.save(Customer.builder()
                            .name("Diana")
                    .build());
        }
        log.info("loadBeerObjects - {}, loadCustomerObjects - {}", beerRepository.findAll(), customerRepository.findAll());
    }
}
