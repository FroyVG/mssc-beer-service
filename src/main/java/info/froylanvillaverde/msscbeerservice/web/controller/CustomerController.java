package info.froylanvillaverde.msscbeerservice.web.controller;


import info.froylanvillaverde.msscbeerservice.web.model.CustomerDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RequestMapping("/api/v1/customer")
@RestController
public class CustomerController {

    @GetMapping("/{customerId}")
    public ResponseEntity<CustomerDto> getCustomerById(@PathVariable UUID customerId){
        return new ResponseEntity<>( CustomerDto
                .builder()
                .id(customerId)
                .name("Froy")
                .build(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity saveNewCustomer(@RequestBody CustomerDto customerDto){

        return new ResponseEntity(HttpStatus.CREATED);
    }

    @PutMapping("/{customerId}")
    public ResponseEntity updateCustomerById(@PathVariable UUID customerId, @RequestBody CustomerDto customerDto){

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
