package info.froylanvillaverde.msscbeerservice.web.mappers;

import info.froylanvillaverde.msscbeerservice.domain.Customer;
import info.froylanvillaverde.msscbeerservice.web.model.CustomerDto;
import org.mapstruct.Mapper;

@Mapper
public interface CustomerMapper {

   CustomerDto customerToCustomerDto(Customer customer);

   Customer customerDtoToCustomer(CustomerDto dto);
}
