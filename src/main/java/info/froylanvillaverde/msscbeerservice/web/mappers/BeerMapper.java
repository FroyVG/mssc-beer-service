package info.froylanvillaverde.msscbeerservice.web.mappers;

import info.froylanvillaverde.msscbeerservice.domain.Beer;
import info.froylanvillaverde.msscbeerservice.web.model.BeerDto;
import org.mapstruct.Mapper;

@Mapper(uses = {DateMapper.class})
public interface BeerMapper {

    BeerDto beerToBeerDto(Beer beer);

    Beer beerDtoToBeer(BeerDto dto);

}
