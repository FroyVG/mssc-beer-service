package info.froylanvillaverde.msscbeerservice.repo;

import info.froylanvillaverde.msscbeerservice.domain.Customer;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.UUID;

public interface CustomerRepository extends PagingAndSortingRepository<Customer, UUID> {

}
